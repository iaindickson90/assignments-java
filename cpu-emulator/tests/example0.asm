# Some sample OSASM instructions - available as ~cisos/example0.asm
MOV %AX , %BX					# Copy AX into BX
  INC     %AX					# Increment AX by one
ADD  $2, %AX					# Increment AX by two
 SUB $5 ,   %AX					# Decrement AX by five
