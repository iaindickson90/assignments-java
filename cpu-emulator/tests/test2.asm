# Illegal test instructions file
MOV $5 $9		# Cannot store value in a constant
MOV			# This instruction needs an argument
CALL _xxx_		# There is no subroutine called xxx
INC $5
MOV $%AX
MOV AX
DEC $5
MOV %IT, $6
MOV $10, ($-1)
SUB BX, AX
MOV $%10, $10
MOV $10, $%10
JMP $5050502
HALT			# Valid instruction