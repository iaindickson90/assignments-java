# This is the first line (=0) which has no code on it 
MOV $65, %AX       # A is 65 decimal in ASCII 
CALL _printchar_   # Print out the value of AX 
INC %AX            # Increment AX by one 
CMP %AX, $91       # Is AX set to one past �Z� yet? (Z is $90) 
JNE $2             # Jump to the third line of the source code 
HALT               # This is the end of the program 