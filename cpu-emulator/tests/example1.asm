MOV $5, %AX        # Setup the registers 
MOV $7, %BX 
MOV $8, %CX 
MOV $2, %DX 
ADD %AX, %BX       # BX = AX + BX = 12 
SUB %DX, %CX       # CX = CX - DX = 6 
ADD %AX, %BX       # BX = AX + BX = 5 + 12 = 17 
INC %CX            # CX = CX + 1 = 7 
HALT               # Stop the CPU now
