MOV $1024, %AX     # Set AX to point to mem address 1024 
MOV $72, (%AX)     # 8-bit copy the H=72 into address 1024 
INC %AX 
MOV $101, (%AX)    # 8-bit copy the e=101 into address 1025 
INC %AX 
MOV $108, (%AX)    # 8-bit copy the l=101 into address 1026 
INC %AX 
MOV $108, (%AX)    # 8-bit copy the l=101 into address 1027 
INC %AX 
MOV $111, (%AX)    # 8-bit copy the o=111 into address 1028 
INC %AX 
MOV $32, (%AX)     # 8-bit copy the � �=32 into address 1029 
INC %AX 
MOV $87, (%AX)     # 8-bit copy the W=87 into address 1030 
INC %AX 
MOV $111, (%AX)    # 8-bit copy the o=111 into address 1031 
INC %AX 
MOV $114, (%AX)    # 8-bit copy the r=114 into address 1032 
INC %AX 
MOV $108, (%AX)    # 8-bit copy the l=108 into address 1033 
INC %AX 
MOV $100, (%AX)    # 8-bit copy the d=100 into address 1034 
INC %AX 
MOV $32, (%AX)     # 8-bit copy the � �=32 into address 1035 
MOV $1024, %AX     # Reset AX back to the start of the string 
MOV $11, %BX       # Set BX to be length of �Hello world�=11 
CALL _printstr_    # Print �Hello world� to the output 
MOV $32, %AX       # Set AX to a space character 
CALL _printchar_   # Print a single space out for padding 
MOV %BX, %AX       # Copy the BX length value into AX 
CALL _printdec_    # Print out the string length in decimal 
HALT 
 
 