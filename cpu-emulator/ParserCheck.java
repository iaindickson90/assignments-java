/*
Copyright (c) 2011, Iain Dickson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Iain Dickson nor the
      names of contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 *
 * @author Torch
 */
public class ParserCheck {
    //The total number of instructions in the file.
    public int numberInstructions;
    
    public void setInstructions(int number){
        numberInstructions = number;
    }

    public void check_operations(Instruction currentInstruction, int currentLine){
        //For each set of instructions there are particular checks that must occur.
        //This checks the command that has been brough, and refers to the checks that must be completed.
            if (currentInstruction.getOperator().equals("MOV")) {
                constant_argument_check(currentInstruction, 2, currentLine + 1);
                correct_argument_check(currentInstruction, 2, currentLine + 1);

            } else if (currentInstruction.getOperator().equals("ADD")) {
                constant_argument_check(currentInstruction, 2, currentLine + 1);
                correct_argument_check(currentInstruction, 2, currentLine + 1);

            } else if (currentInstruction.getOperator().equals("SUB")) {
                constant_argument_check(currentInstruction, 2, currentLine + 1);
                correct_argument_check(currentInstruction, 2, currentLine + 1);

            } else if (currentInstruction.getOperator().equals("INC")) {
                constant_argument_check(currentInstruction, 1, currentLine + 1);
                correct_argument_check(currentInstruction, 1, currentLine + 1);
                
            } else if (currentInstruction.getOperator().equals("DEC")) {
                constant_argument_check(currentInstruction, 1, currentLine + 1);
                correct_argument_check(currentInstruction, 1, currentLine + 1);
                
                
            } else if (currentInstruction.getOperator().equals("CMP")) {
                correct_argument_check(currentInstruction, 2, currentLine + 1);
                  
            }
    }
    
    private void constant_argument_check(Instruction currentInstruction, int argument, int currentLine) {
        //If there is one argument, make sure that there is not a constant in the first argument.
        if (argument == 1 && !currentInstruction.getOperand1().isEmpty() && currentInstruction.getOperand1().charAt(0) == '$') {
            System.out.println("Error (Line " + currentLine + "): Argument 1 cannot be a constant.");
        }
        //If there are two arguments, make sure the constant is not in the second argument
        if (argument == 2 && !currentInstruction.getOperand2().isEmpty() && currentInstruction.getOperand2().charAt(0) == '$') {
            System.out.println("Error (Line " + currentLine + "): Argument 2 cannot be a constant.");
        }
    }

    private void correct_argument_check(Instruction currentInstruction, int args, int currentLine) {
        //If there are 1 or 2 arguments, check the first argument.
        if (args == 1 || args == 2) {
            //Try these things, just in case the argument does not exist.
            try {
                //If it's a register, does it provide a correct register?
                if (currentInstruction.getOperand1().charAt(0) == '%' && !currentInstruction.getOperand1().matches("%[A-D]X")) {
                    System.out.println("Error (Line " + currentLine + "): Argument 1 does not have a correctly defined register");
                    //if it's a constant, is it a digit?
                } else if (currentInstruction.getOperand1().charAt(0) == '$' && !currentInstruction.getOperand1().matches("\\$\\d\\d*")) {
                    System.out.println("Error (Line " + currentLine + "): Argument 1 does not have a correctly defined constant");
                    //If it's a memory address, is it defined correctly?
                } else if (currentInstruction.getOperand1().charAt(0) == '(' && !currentInstruction.getOperand1().matches("\\(\\$\\d\\d*\\)|\\(%[A-D]X\\)")) {
                    System.out.println("Error (Line " + currentLine + "): Argument 1 does not have a correctly defined memory reference");
                    //Correct versions
                } else if (currentInstruction.getOperand1().matches("%[A-D]X") || currentInstruction.getOperand1().matches("\\$\\d\\d*") || currentInstruction.getOperand1().matches("\\(\\$\\d\\d*\\)|\\(%[A-D]X\\)")) {
                } else {
                    //Argument has something defined, but it's not correct.
                    System.out.println("Error (Line " + currentLine + "): Argument 1 does not have a register, constant, or memory reference defined correctly.");
                }
            } catch (Exception e) {
                //Non defined argument
                System.out.println("Error (Line " + currentLine + "): Argument 1 has not been defined.");
            }
        }
        if (args == 2) {
            try {
                //Check that a comma is in the right place.
                if (!currentInstruction.getOriginal().matches(".*[$|%|\\(.*\\)].*\\s*,\\s*[$|%|\\(.*\\)].*\\s*$|.*\\s.*\\s*,.*")){
                    System.out.println("Error (Line " + currentLine + "): Instruction not correctly formed, does not contain ',' between arguments.");
                }
                //If it's a register, check it's letters.
                if (currentInstruction.getOperand2().charAt(0) == '%' && !currentInstruction.getOperand2().matches("%[A-D]X")) {
                    System.out.println("Error (Line " + currentLine + "): Argument 2 does not have a correctly defined register");
                    //Constant check for digits.
                } else if (currentInstruction.getOperand2().charAt(0) == '$' && !currentInstruction.getOperand2().matches("\\$\\d\\d*")) {
                    System.out.println("Error (Line " + currentLine + "): Argument 2 does not have a correctly defined constant");
                    //Check for memory addresses
                } else if (currentInstruction.getOperand2().charAt(0) == '(' && !currentInstruction.getOperand2().matches("\\(\\$\\d\\d*\\)|\\(%[A-D]X\\)")) {
                    System.out.println("Error (Line " + currentLine + "): Argument 2 does not have a correctly defined memory reference");
                    //Correct values
                } else if (currentInstruction.getOperand2().matches("%[A-D]X") || currentInstruction.getOperand2().matches("\\$\\d\\d*") || currentInstruction.getOperand2().matches("\\(\\$\\d\\d*\\)|\\(%[A-D]X\\)")) {
                } else {
                    //Argument defined, but not correctly.
                    System.out.println("Error (Line " + currentLine + "): Argument 2 does not have a register, constant, or memory reference defined correctly.");
                }
            } catch (Exception e) {
                //Non defined argument.
                System.out.println("Error (Line " + currentLine + "): Argument 2 has not been defined.");
            }
        }
    }

}
