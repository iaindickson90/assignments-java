/*
Copyright (c) 2011, Iain Dickson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Iain Dickson nor the
      names of contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 *
 * @author dicic001
 */
import java.util.*;
import java.io.*;

public class Emulator {
    //Array list to hold the instructions
    private ArrayList<Instruction> instructions = new ArrayList<Instruction>();
    //The memory object.
    private Memory ASMMemory = new Memory();
    //ParserCheck is used by each method when an error crops up.
    private ParserCheck check = new ParserCheck();

    public Emulator(String asmfile) {
        //Try, and if fail, error out.
        try {
            //Create the objects to read from the file.
            FileReader fr = new FileReader(asmfile);
            BufferedReader br = new BufferedReader(fr);
            //Put line contents into string for parsing.
            String nextLine = br.readLine();
            //While the line is not null, keep adding to the instructionlist.
            while (nextLine != null) {
                instructions.add(new Instruction(nextLine));
                nextLine = br.readLine();
            }
            fr.close();
            br.close();
        } catch (Exception e) {
            System.out.println("Error (Startup): File does not exist.");
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        //check for arguments.
        //String asmfile = "test2.asm";
        //Create an object of yourself, and run the run() command.
        String asmfile = "";
        if((args.length > 0)){
            asmfile = args[0];
        } else {
            System.out.println("Error (Startup): No file selected.");
            System.exit(0);
        }
        Emulator emulator1 = new Emulator(asmfile);
        emulator1.run();
    }

    private void run() {
        Instruction currentInstruction;
        check.setInstructions(instructions.size());
        //While there are still commands left to run, according to the IP int.
        while(ASMMemory.getIP() < instructions.size()){ //for (int x = 0; x < instructions.size(); x++) {
            currentInstruction = instructions.get(ASMMemory.getIP());
            //Get the instruction
            //Can't use switch, so have to use if statements. For each statement, check it is correct, run the appropriate command, then increase IP.
            if (currentInstruction.getOperator().equals("NOP")) {
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().equals("HALT")) {
                print(currentInstruction);
                System.exit(1);

            } else if (currentInstruction.getOperator().equals("MOV")) {
                move(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().equals("ADD")) {
                add(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().equals("SUB")) {
                sub(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().equals("INC")) {
                inc(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().equals("DEC")) {
                dec(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().charAt(0) == 'J') {
                jump(currentInstruction);

            } else if (currentInstruction.getOperator().equals("CMP")) {
                cmp(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);

            } else if (currentInstruction.getOperator().equals("CALL")) {
                call(currentInstruction);
                ASMMemory.setIP(ASMMemory.getIP() + 1);
                
            } else {
                //If command is not a correct one.
                System.out.println("Error (Line " + (ASMMemory.getIP()) + "):Command does not conform to the OSASM Spec.");
                ASMMemory.setIP(ASMMemory.getIP() + 1);
            }
            //Print the memory references, and the line that was just run.
            print(currentInstruction);

        }
    }

    private void move(Instruction currentInstruction) {
        int toMove = 0;
        //Regex test to check if the line is in the correct form. Need to implement extra error checking.
        String test = "\\s*MOV?\\s*(([%][A-D]X)|([$]\\d\\d*)|([(][%][A-D]X[)])|([(][$]\\d\\d*[)]))\\s*[,]\\s*(([(][%][A-D]X[)])|([(][$]\\d\\d*[)])|([%][A-D]X))\\s*";
        if (test(currentInstruction, test)) {
            //Get the data from the first operand, and set it in the second operand.
            toMove = ASMMemory.getData(currentInstruction.getOperand1());
            ASMMemory.setData(currentInstruction.getOperand2(), toMove);
        } else {
            //If the operation does not follow the regex above, put it through a number of tests to figure out what's wrong with it.
            check.check_operations(currentInstruction, ASMMemory.getIP());
        }
    }

    private void add(Instruction currentInstruction) {
        //Variable to store the first int to be added.
        int firstDigit = 0;
        int secondDigit = 0;
        //Run the regex test.
        String test = "\\s*ADD?\\s*(([%][A-D]X)|([$]\\d\\d*)|([(][%][A-D]X[)])|([(][$]\\d\\d*[)]))\\s*[,]\\s*(([(][%][A-D]X[)])|([(][$]\\d\\d*[)])|([%][A-D]X))\\s*";
        if (test(currentInstruction, test)) {
            firstDigit = ASMMemory.getData(currentInstruction.getOperand1());
            secondDigit = ASMMemory.getData(currentInstruction.getOperand2());
            ASMMemory.setData(currentInstruction.getOperand2(), firstDigit + secondDigit);
        } else {
            //If regex check fails, run the check operations.
            check.check_operations(currentInstruction, ASMMemory.getIP());
        }
    }
    
    private void sub(Instruction currentInstruction) {
        //Variable to store the first int.
        int firstDigit = 0;
        //Variable to store the second int.
        int secondDigit = 0;
        //Test the string
        String test ="\\s*SUB?\\s*(([%][A-D]X)|([$]\\d\\d*)|([(][%][A-D]X[)])|([(][$]\\d\\d*[)]))\\s*[,]\\s*(([(][%][A-D]X[)])|([(][$]\\d\\d*[)])|([%][A-D]X))\\s*";
        if (test(currentInstruction, test)) {
            //Get the data from the first and second digits
            firstDigit = ASMMemory.getData(currentInstruction.getOperand1());
            secondDigit = ASMMemory.getData(currentInstruction.getOperand2());
            //Subtract them, then set the data with the value.
            ASMMemory.setData(currentInstruction.getOperand2(), secondDigit - firstDigit);
        } else {
            //If regex check fails, run the check operations.
            check.check_operations(currentInstruction, ASMMemory.getIP());
        }
    }
    
    private void inc(Instruction currentInstruction) {
        int firstDigit = 0;
        //Run the test
        String test ="\\s*INC?\\s*(([%][A-D]X)|([(][%][A-D]X[)])|([(][$]\\d\\d*[)]))\\s*";
        if (test(currentInstruction, test)) {
            //Get the digit that has to be incremented, and then set it after it has been.
            firstDigit = ASMMemory.getData(currentInstruction.getOperand1());
            ASMMemory.setData(currentInstruction.getOperand1(), firstDigit + 1);
        } else {
            //If regex check fails, run the check operations.
            check.check_operations(currentInstruction, ASMMemory.getIP());
        }
    }

    private void dec(Instruction currentInstruction) {
        int firstDigit = 0;
        //Test the command
        String test = "\\s?DEC?\\s*(([%][A-D]X)|([(][%][A-D]X[)])|([(][$]\\d\\d*[)]))\\s*";
        if (test(currentInstruction, test)) {
            //Get the digit, then subtract 1, then set it in the same memory address.
            firstDigit = ASMMemory.getData(currentInstruction.getOperand1());
            ASMMemory.setData(currentInstruction.getOperand1(), firstDigit - 1);
        } else {
            //If regex check fails, run the check operations.
            check.check_operations(currentInstruction, ASMMemory.getIP());
        }
    }

    private void jump(Instruction currentInstruction) {
        String test = "\\s?(JMP|JE|JNE|JL|JG|JLE|JGE)?\\s*(([$]\\d\\d*))\\s*";
        //Run the test
        if (test(currentInstruction, test)) {
            //Check which of the jump functions is being called, and test the specific information to do with it.
            if ((Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", ""))) <= (instructions.size() - 1)) {
                if (currentInstruction.getOperator().equals("JMP")) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else if (currentInstruction.getOperator().equals("JE") && (ASMMemory.getFLAGS() == 1)) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else if (currentInstruction.getOperator().equals("JNE") && (ASMMemory.getFLAGS() != 1)) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else if (currentInstruction.getOperator().equals("JL") && (ASMMemory.getFLAGS() == -1)) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else if (currentInstruction.getOperator().equals("JG") && (ASMMemory.getFLAGS() == 2)) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else if (currentInstruction.getOperator().equals("JG") && ((ASMMemory.getFLAGS() == 2) || (ASMMemory.getFLAGS() == 1))) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else if (currentInstruction.getOperator().equals("JG") && ((ASMMemory.getFLAGS() == -1) || (ASMMemory.getFLAGS() == 1))) {
                    ASMMemory.setIP(Integer.parseInt(currentInstruction.getOperand1().replaceAll("[($)]", "")));
                } else {
                    //If the previous commands do not apply, just add one to the IP.
                    ASMMemory.setIP(ASMMemory.getIP() + 1);
                }
            } else {
                System.out.println("Error (" + ASMMemory.getIP() + "): Jump instruction is out of range. Please change to a value that is within the program.");
                ASMMemory.setIP(ASMMemory.getIP() + 1);
            }
        }
    }

    private void cmp(Instruction currentInstruction){
        //Variables for first and second digit.
        int firstDigit = 0;
        int secondDigit = 0;
        //Test it
        String test = "\\s*CMP?\\s*(([%][A-D]X)|([$]\\d\\d*)|([(][%][A-D]X[)])|([(][$]\\d\\d*[)]))\\s*[,]\\s*(([(][%][A-D]X[)])|([(][$]\\d\\d*[)])|([$]\\d\\d*)|([%][A-D]X))\\s*";
        if (test(currentInstruction, test)) {
            //Get the two digits.
            firstDigit = ASMMemory.getData(currentInstruction.getOperand1());
            secondDigit = ASMMemory.getData(currentInstruction.getOperand2());
            //Compare them using the particular settings.
            //1 is equal, -1 is less than, 2 is greater than
            if(firstDigit == secondDigit){
                ASMMemory.setFLAGS(1);
            } else if (firstDigit > secondDigit) {
                ASMMemory.setFLAGS(-1);
            } else if (firstDigit < secondDigit) {
                ASMMemory.setFLAGS(2);
            }
        } else {
            check.check_operations(currentInstruction, ASMMemory.getIP());
        }
    }
    
    private void call(Instruction currentInstruction){
        //Run the test
        String test = "\\s?CALL?\\s*_print(str|char|dec)_\\s*";
        if (test(currentInstruction, test)) {
            //Check which call function is actually being called.
            if (currentInstruction.getOperand1().equals("_printstr_")) {
                //For printstr, run a loop till all the characters have been printed.
                int address = ASMMemory.getAX();
                for(int x = 0; x < ASMMemory.getBX(); x++){
                    System.out.print((char) ASMMemory.getAddress(address));
                    address++;
                }
                //Print out the particular character.
            } else if (currentInstruction.getOperand1().equals("_printchar_")) {
                System.out.print((char)(ASMMemory.getAX()));
                //Print out the integer of the data
            } else if (currentInstruction.getOperand1().equals("_printdec_")) {
                System.out.print((int) ASMMemory.getAX());
            }
        } else {
            System.out.println("Error (" + ASMMemory.getIP() + "): Call instruction is not defined properly. Please use _print[str/char/dec]_");
        }
    }

    private void error(String error){
        
    }

    private void print(Instruction currentInstruction) {
        System.out.println("#####");
        System.out.println("Instruction: " + currentInstruction.getOriginal());
        System.out.println("AX: " + ASMMemory.getAX());
        System.out.println("BX: " + ASMMemory.getBX());
        System.out.println("CX: " + ASMMemory.getCX());
        System.out.println("DX: " + ASMMemory.getDX());
        System.out.println("IP: " + ASMMemory.getIP());
        String flags = "";
        //Print the correct format for the flags.
        if(ASMMemory.getFLAGS() == 1){
            flags = "1  0  0";
        } else if(ASMMemory.getFLAGS() == -1){
            flags = "0  0  1";
        } else if(ASMMemory.getFLAGS() == 2){
            flags = "0  1  0";
        }
        System.out.println("FLAGS(EQ/GT/LT): " + ASMMemory.getFLAGS());
        System.out.println();
    }

    public boolean test(Instruction currentInstruction, String regex) {
        //If it does not fit the regex, return false automatically.
        if (!currentInstruction.getOriginal().matches(regex)) {
            return false;
        }
        //if it has arguments
        if (currentInstruction.getOperands() > 0) {
            //Check whether they refer to memory addresses, and store them in operand1
            int operand1 = 0;
            if (currentInstruction.getOperand1().substring(0, 2).equals("($")) {
                operand1 = Integer.parseInt(currentInstruction.getOperand2().replaceAll("[($)]", ""));
            }
            if (currentInstruction.getOperand1().substring(0, 2).equals("(%")) {
                if (currentInstruction.getOperand2().substring(0, 2).equals("(%AX)")) {
                    operand1 = ASMMemory.getAddress(ASMMemory.getAX());
                } else if (currentInstruction.getOperand2().equals("(%BX)")) {
                    operand1 = ASMMemory.getAddress(ASMMemory.getBX());
                } else if (currentInstruction.getOperand2().equals("(%CX)")) {
                    operand1 = ASMMemory.getAddress(ASMMemory.getCX());
                } else if (currentInstruction.getOperand2().equals("(%DX)")) {
                    operand1 = ASMMemory.getAddress(ASMMemory.getDX());
                }
            }
            //If the memory address is out of bounds, return error.
            if (!((operand1 < 65536) && (operand1 > -1))) {
                System.out.println("Error (Line " + (ASMMemory.getIP()) + "): Argument 1 has Memory out of Bounds");
                return false;
            }
        }
        if (currentInstruction.getOperands() == 2) {
            int operand2 = 0;
            String test = currentInstruction.getOperand2().substring(0, 2);
            //Check if anything stores memory addresses
            if (currentInstruction.getOperand2().substring(0, 2).equals("($")) {
                operand2 = Integer.parseInt(currentInstruction.getOperand2().replaceAll("[($)]", ""));
            }
            if (currentInstruction.getOperand2().substring(0, 2).equals("(%")) {
                if (currentInstruction.getOperand2().equals("(%AX)")) {
                    operand2 = ASMMemory.getAddress(ASMMemory.getAX());
                } else if (currentInstruction.getOperand2().equals("(%BX)")) {
                    operand2 = ASMMemory.getAddress(ASMMemory.getBX());
                } else if (currentInstruction.getOperand2().equals("(%CX)")) {
                    operand2 = ASMMemory.getAddress(ASMMemory.getCX());
                } else if (currentInstruction.getOperand2().equals("(%DX)")) {
                    operand2 = ASMMemory.getAddress(ASMMemory.getDX());
                }
            }
            //if larger or less than, return out of bounds error.
            if (!((operand2 < 65536) && (operand2 > -1))) {
                System.out.println("Error (Line " + (ASMMemory.getIP()) + "): Argument 2 has Memory out of Bounds");
                return false;
            }
        }

        return true;
    }
}
