/*
Copyright (c) 2011, Iain Dickson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Iain Dickson nor the
      names of contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

public class Memory {
//(byte) (0xff & yourInt)
//will get the first 8 bits of the int
//and store it as a byte
    //Registers, flags, and byte array.
    private int AX;
    private int BX;
    private int CX;
    private int DX;
    private int IP;
    private int FLAGS;
    private byte[] RAM = new byte[65536];

    //Constructor, creates objects.
    public Memory(){
        AX = 0;
        BX = 0;
        CX = 0;
        DX = 0;
        IP = 0;
        FLAGS = 0;
    }

    public int getAX() {
        return AX;
    }

    public int getBX() {
        return BX;
    }

    public int getCX() {
        return CX;
    }

    public int getDX() {
        return DX;
    }

    public int getIP(){
        return IP;
    }

    public int getFLAGS(){
        return FLAGS;
    }

    //Get the address
    public byte getAddress(int reference){
        //Check that it's within range. Really this is not needed.
        if((reference < 65536) && (reference > -1)){
            return RAM[reference - 1];
        }
        return (byte)(0xff);
    }

    public void setAX(int newAX) {
        AX = newAX;
    }

    public void setBX(int newBX) {
        BX = newBX;
    }

    public void setCX(int newCX) {
        CX = newCX;
    }

    public void setDX(int newDX) {
        DX = newDX;
    }


    public void setIP(int newIP){
        IP = newIP;
    }

    public void setFLAGS(int newFLAGS){
        FLAGS = newFLAGS;
    }

    public void setAddress(int reference, byte data){
        //Final check whether it is in bounds. Really this is not needed.
        if((reference < 65536) && (reference > -1)){
            RAM[reference - 1] = data;
        }
    }

    public int getData(String reference) {
        //If it refers to a register.
        if (reference.charAt(0) == '%') {
            //Get the memory out of that register, return it.
            if (reference.equals("%AX")) {
                return getAX();
            } else if (reference.equals("%BX")) {
                return getBX();
            } else if (reference.equals("%CX")) {
                return getCX();
            } else if (reference.equals("%DX")) {
                return getDX();
            }
        //If it's a constant, parse the constant.
        } else if (reference.charAt(0) == '$') {
            return Integer.parseInt(reference.substring(1));
        //If it refers to RAM space, extract that from the RAM using the REGISTERS or the CONSTANT
        } else if (reference.charAt(0) == '(') {
            if (reference.charAt(1) == '%') {
                if (reference.equals("(%AX)")) {
                    return getAddress(getAX());
                } else if (reference.equals("(%BX)")) {
                    return getAddress(getBX());
                } else if (reference.equals("(%CX)")) {
                    return getAddress(getCX());
                } else if (reference.equals("(%DX)")) {
                    return getAddress(getDX());
                }
            } else {
                //Get the constant out by replacing the brackets and $
                return getAddress(Integer.parseInt(reference.replaceAll("[($)]", "")));
            }
            //
        }
        return -1;
    }

    public void setData(String reference, int data) {
        //If the reference refers to a register, set that register to the incoming data.
        if (reference.charAt(0) == '%') {
                if (reference.equals("%AX")) {
                    setAX(data);
                } else if (reference.equals("%BX")) {
                    setBX(data);
                } else if (reference.equals("%CX")) {
                    setCX(data);
                } else if (reference.equals("%DX")) {
                    setDX(data);
                }
            //If the second operand is a memory reference
            } else if (reference.charAt(0) == '(') {
                //If it's a register holding the address
                if (reference.charAt(1) == '%') {
                    //Move the data into the reference specified by the register, and correctly put it in.
                    if (reference.equals("(%AX)")) {
                        setAddress(getAX(), (byte) (0xff & data));
                    } else if (reference.equals("(%BX)")) {
                        setAddress(getBX(), (byte) (0xff & data));
                    } else if (reference.equals("(%CX)")) {
                       setAddress(getCX(), (byte) (0xff & data));
                    } else if (reference.equals("(%DX)")) {
                        setAddress(getDX(), (byte) (0xff & data));
                    }
                    //If its a constant holding the address
            } else {
                //Set the address defined by the constant to the value required.
                setAddress(Integer.parseInt(reference.replaceAll("[($)]", "")), (byte) (0xff & data));
            }
        }
    }
}
