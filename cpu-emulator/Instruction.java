/*
Copyright (c) 2011, Iain Dickson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Iain Dickson nor the
      names of contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import java.util.*;
/**
 *
 * @author dicic001
 */
public class Instruction {

    //The operation in the instruction
    private String operation;
    //The first operand
    private String operand1 = "";
    //The second operand
    private String operand2 = "";
    //Number of operands
    private int operands;
    //Original instruction, without comment.
    private String original;
    //public  String comment;

    public Instruction() {
    }
    
    public Instruction (String instruction) {
        StringTokenizer st;
        //Take out "," and comments.
        String line = instruction.replaceAll("[,]|(#)(.*)($)", "");
        //Break the string up into parts using white space
        st = new StringTokenizer(line);
        if (line.equals("")){
            operation = "NOP";
            original = "NOP";
        }
        //Put into the object depending on how many arguments there are.
        switch(st.countTokens()){
            //For single commands, such as HALT
            case 1:
                operation = st.nextToken();
                operands = 0;
                original = instruction.replaceAll("(#)(.*)($)", "");
                break;
            //For commands such as INC
            case 2:
                operation = st.nextToken();
                operand1 = st.nextToken();
                operands = 1;
                original = instruction.replaceAll("(#)(.*)($)", "");
                break;
                //Commands such as MOV
            case 3:
                operation = st.nextToken();
                operand1 = st.nextToken();
                operand2 = st.nextToken();
                operands = 2;
                original = instruction.replaceAll("(#)(.*)($)", "");
                break;
            default:
                break;
        }
    }
    
    public String toString() {
        //Prints out the operation and operands for testing purposes.
        return operation + " " + operand1 + " " + operand2;
    }

    //Get the operator.
    public String getOperator() {
        return operation;
    }

    //Get the first operand
    public String getOperand1() {
        return operand1;
    }

    //Get the second operand
    public String getOperand2() {
        return operand2;
    }

    //Get the number of operands
    public int getOperands() {
        return operands;
    }

    //Get the original instruction, minus comments. 
    public String getOriginal(){
        return original;
    }
}
