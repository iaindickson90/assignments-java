/*
Copyright (c) 2011, Iain Dickson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Iain Dickson nor the
      names of contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 *
 * @author Torch
 */
import java.io.*;
import java.net.*;
import java.util.*;

public class Server extends Thread {
    //Used by the main process
    int port;
    ServerSocket serverSocket;
    static ArrayList<Server> array = new ArrayList<Server>();

    //Used by the threads.
    Socket socket1;
    boolean connected;
    String login = "";
    PrintWriter out = null;
    BufferedReader in = null;

    //Create the object, and listen for the connections
    public static void main(String args[]) {
        if ((args.length > 0)) {
            //Server server1 = new Server("9999");
            Server server1 = new Server(args[0]);
            server1.listen();
        } else {
            System.out.println("ERROR No port selected.");
            System.exit(0);
        }
    }

    //Constructor for the initial object
    public Server(String port) {
        this.port = Integer.parseInt(port);
    }

    //Constructor for all threads
    public Server(Socket connection) {
        this.socket1 = connection;
    }

    //Listen for new connections
    public void listen() {
        try {
            //try creating new serversocket, and listening
            serverSocket = new ServerSocket(port);
            while (true) {
                //add new sockets to the array, and start the threads.
                array.add(new Server(serverSocket.accept()));
                System.out.println("CONNECTION");
                array.get(array.size() - 1).start();

            }
        } catch (Exception e) {
            close();
        }
    }

    public void run() {
        try {
            //Create the reader and writer, and initiate contact with the client.
            connected = true;
            in = new BufferedReader(new InputStreamReader(socket1.getInputStream()));
            out = new PrintWriter(new DataOutputStream(socket1.getOutputStream()));
            out.println("WELCOME Welcome to the server");
            out.flush();

            String str;
            while (connected) {
                if (in.ready()) {
                    if ((str = in.readLine()) != null) {
                        StringTokenizer st;
                        st = new StringTokenizer(str);
                        String instruction = st.nextToken();
                        //Test that it's a login, and test if it's correct
                        if (instruction.equals("LOGIN") && test(str, "LOGIN\\s\\w*")) {
                            System.out.println(str);
                            //Test if the login is being used by any other client
                            if(test_login(str = st.nextToken())){
                                login = str;
                                //Print the OK message
                                out.println("LOGIN_OK");
                                out.flush();
                            } else {
                                //Print the fail message
                                out.println("ERROR Username already in use");
                                out.flush();
                                close();
                            }
                            //Check if it's a draw message and send it to all clients.
                        } else if (instruction.equals("DRAW") && test(str, "DRAW\\s\\d*\\s\\d*\\s\\d*\\s\\d*\\s(black|white|red|blue|green|yellow|magenta|cyan)")) {
                            System.out.println(str);
                            draw_message(str);
                            //Check if it's an error, if it is close the socket.
                        } else if (instruction.equals("ERROR")) {
                            System.out.println(str);
                            close();
                        } else {
                            out.println("ERROR Unrecognised command, please reconnect and try again.");
                            out.flush();
                            System.out.println("ERROR Unrecognised command, please reconnect and try again.");
                            close();
                        }
                    }
                }
            }
        } catch (Exception e) {
            close();
        }
    }

    //Synchronized to ensure that only one socket can access the array at one time.
    public synchronized boolean test_login(String login){
        Server temp;
        //Check each member of the array to see if any of them are connected, and logged in with the same name
        for(int a = 0; a < array.size(); a++){
            temp = array.get(a);
            if(temp.connected && temp.login.equals(login)){
                return false;
            }
        }
        return true;
    }

    //Synchronized to ensure that only one socket can access the array at one time.
    public synchronized void draw_message(String message){
        Server temp;
        //For each connected array member, send the drawmsg.
        for(int a = 0; a < array.size(); a++){
            temp = array.get(a);
            if (temp.connected) {
                temp.out.println("DRAWMSG " + this.login + message.replaceAll("DRAW", ""));
                temp.out.flush();
            }
        }
    }

    //Closes everything correctly.
    public void close() {
        connected = false;
        try {
            //If it's null, close the bufferedreader
            if (in != null) {
                in.close();
            }

            //if it's null, close the printwriter
            if (out != null) {
                out.close();
            }

            //if it's null, close the socket.
            if (socket1 != null) {
                socket1.close();
            }

            //Remove the object from the array.
            Server temp;
            for (int a = 0; a < array.size(); a++) {
                temp = array.get(a);
                if (this.equals(temp)) {
                    array.remove(a);
                }
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //test if the instruction with the regex.
    public boolean test(String currentInstruction, String regex) {
        //If it does not fit the regex, return false automatically.
        if (!currentInstruction.matches(regex)) {
            return false;
        }
        return true;
    }
}
//Create buffer, check buffer after recieving data, add stuff to buffer.
