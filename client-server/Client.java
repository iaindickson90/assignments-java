/*
Copyright (c) 2011, Iain Dickson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Iain Dickson nor the
      names of contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/**
 *
 * @author Torch
 */
import java.io.*;
import java.net.*;
import java.util.*;

public class Client extends Thread {

    //Statics used by both threads.
    private static Socket socket1 = null;
    private static PrintWriter out = null;
    private static BufferedReader in = null;
    private static boolean connected;

    //Used by the initial thread.
    private String hostname;
    private String port;
    private static String login;

    //Constructor used to create object in main.
    public Client(String hostname, String port, String login) {
        this.hostname = hostname;
        this.port = port;
        this.login = login;
    }
    //Used by thread creation.
    public Client() {
    }

    //Closes everything correctly.
    public void close() {
        connected = false;
        try {
            //If it's null, close the bufferedreader
            if (in != null) {
                in.close();
            }

            //if it's null, close the printwriter
            if (out != null) {
                out.close();
            }

            //if it's null, close the socket.
            if (socket1 != null) {
                socket1.close();
            }
            
        } catch (Exception e) {
            System.out.println(e);
        }
        System.exit(0);
    }

    //Reading from the terminal.
    public void read() {
        //Create a scanner object.
        Scanner scanner = new Scanner(System.in);
        String instruction = "";
        //Continually check for user input while connected.
        while (connected) {
            instruction = scanner.nextLine();
            //if it's exit, close it.
            if (instruction.equals("EXIT")) {
                close();
            } else {
                //Print the instruction to the socket.
                print(instruction);
            }
        }
    }
    //Thread run method.
    public void run() {
        try {
            String str;
            //while the socket is connected
            while (connected) {
                if (in.ready()) {
                    //Check that it's available to read from
                    if ((str = in.readLine()) != null) {
                        StringTokenizer st;
                        st = new StringTokenizer(str);
                        String instruction = st.nextToken();
                        //If the instruction is welcome, and fits the test, print the login details.
                        if (instruction.equals("WELCOME") && test(str, "WELCOME\\s(.*)")) {
                            System.out.print(str);
                            print("LOGIN " + login);
                            
                        //If it's a login code, print it out.
                        } else if (instruction.equals("LOGIN_OK") && test(str, "LOGIN_OK")) {
                            System.out.print("\n" + str + "\n--->");

                        //If it's a drawmsg, print it out.
                        } else if (instruction.equals("DRAWMSG") && test(str, "DRAWMSG\\s\\w*\\s\\d*\\s\\d*\\s\\d*\\s\\d*\\s(black|white|red|blue|green|yellow|magenta|cyan)")) {
                            System.out.print("\n" + str + "\n--->");

                        //If it's an error, print it out and close all connections.
                        } else if (instruction.equals("ERROR")) {
                            System.out.print("\n" + str + "\n--->");
                            close();
                        } else {

                            //Command not recognised, close all.
                            System.out.println("ERROR Unrecognised command or command structure, server deformed");
                            close();
                        }
                    }
                }
            }
        } catch (Exception e) {
            //Massive error, close all.
            System.out.println("ERROR Unrecognised command, server deformed");
            close();
        }

    }

    public static void main(String args[]) {
        if ((args.length > 0)) {
            //Create client objecct from args. Include test for numbers.
            Client client1 = new Client(args[0], args[1], args[2]);
            //Client client1 = new Client("cis-lab.ml.unisa.edu.au", "9999", "iaindickson");
            client1.connect();
        } else {
            System.out.println("ERROR Wrong number of arguments");
            System.exit(0);
        }
    }

    public void connect() {
        try {
            //socket1 = new Socket("cis-lab.ml.unisa.edu.au", 9999);
            //Try creating socket.
            socket1 = new Socket(hostname, Integer.parseInt(port));
            if (socket1.isConnected()) {
                //Implement Print and Read
                out = new PrintWriter(new DataOutputStream(socket1.getOutputStream()));
                in = new BufferedReader(new InputStreamReader(socket1.getInputStream()));
                connected = true;

                //Create new thread, and run it.
                Thread client1 = new Client();
                client1.start();
                //Wait a second, then start reading from terminal.
                Thread.sleep(1000);
                read();
            }


        } catch (Exception e) {
            //Error occurs, close everything.
            System.out.println(e);
            close();
        }
    }
    
    //Synchronized method to print out to socket.
    public synchronized void print(String message){
        out.println(message);
        out.flush();
    }

    //Test the instruction against the regex.
    public boolean test(String currentInstruction, String regex) {
        //If it does not fit the regex, return false automatically.
        if (!currentInstruction.matches(regex)) {
            return false;
        }
        return true;
    }
}
